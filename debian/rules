#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# LLVM 6.0 headers produce lots of class-memaccess warnings
export DEB_CXXFLAGS_MAINT_APPEND = -Wno-class-memaccess

archs_that_need_atomic = armel
ifeq ($(DEB_HOST_ARCH),$(findstring $(DEB_HOST_ARCH), $(archs_that_need_atomic)))
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--no-as-needed -latomic -Wl,--as-needed
endif

# wrong size reported, although the test correctly identifies the issue
# https://bugs.kde.org/show_bug.cgi?id=413169
ifeq ($(DEB_HOST_ARCH),i386)
RUN_TESTS_ARGS += --exclude inefficient-qlist
endif

# keep the "++" at the end, to make sure the tool used is the C++ frontend
CLANGPP=$(shell realpath $$(env PATH=$$(echo "$$PATH" | sed -r -e 's|/usr/lib/ccache/?:||') which clang++))++

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- -DCLANGPP_EXECUTABLE="$(CLANGPP)"

override_dh_auto_install:
	dh_auto_install
	# drop RPATH pointing to the private libdir of LLVM libraries,
	# i.e. /usr/lib/llvm-$VERSION/lib
	chrpath -d $(CURDIR)/debian/clazy/usr/bin/clazy-standalone $(CURDIR)/debian/clazy/usr/lib/*/*.so
	# drop extra license file
	rm $(CURDIR)/debian/clazy/usr/share/doc/clazy/COPYING-LGPL2.txt

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	chmod +x $(CURDIR)/obj-$(DEB_HOST_GNU_TYPE)/clazy
	cd tests && env PATH=$(CURDIR)/obj-$(DEB_HOST_GNU_TYPE):$(CURDIR)/obj-$(DEB_HOST_GNU_TYPE)/bin:$(PATH) LD_LIBRARY_PATH=$(CURDIR)/obj-$(DEB_HOST_GNU_TYPE)/lib DEB_HOST_ARCH=$(DEB_HOST_ARCH) CCACHE_DISABLE=1 ./run_tests.py --verbose $(RUN_TESTS_ARGS)
endif

override_dh_compress:
	# do not compress documentation markdown & JSON files
	dh_compress -X.md -X.json

override_dh_gencontrol:
	dh_gencontrol -- -V'clazy:clang=$(shell dpkg -S "$(CLANGPP)" | cut -d: -f1)'
